TFL Portal
==============

How to build and run the code
===========
On initial load (SplashVC > CredentialVC)  then enter appId and developerKey and hit continue to HomeVC.


Subsequent Load
=============
Subsequent load will load the HomeVC.
Click on the arrow on the top of HomeVC to change appId and developerKey.


Test
======
Simple HomeVC and CredentialVC test in the test folder.

Assumptions
==========
The appId and developerKey are stored in coredata for securtiy as against Userdefault.


Anomalies
========
Success and failure responses are different (i.e. Array & Dictionary respectively).
Invalid appId or developerKey returns HTML markup error response or string literals


More Implemenation
================

1. Requires Reachability.
2. Better Unit test.
3. Better UI design.
