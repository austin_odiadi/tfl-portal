//
//  JSONDecodable.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 04/08/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//

import Foundation

protocol JSONDecodable {
    
    init?(data: JSON)
    
}
