//
//  RequestParams.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 04/08/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//

import UIKit

enum HTTPMethod: Int {
    case GET
    case PUT
    case POST
    case DELETE
};

class RequestParams {
    
    var body: JSONData?
    var baseURL: String?
    var endpoint: String?
    var HTTPMethod: HTTPMethod?
    var headers: JSONData?
    var miscellaneous: JSONData?
    
     init(baseURL: String, body: JSONData? = nil, headers: JSONData? = nil) {
        
        if let bdy = body {
            self.body = bdy
        }
        
        self.baseURL = baseURL
        
        if let hdrs = headers {
            self.headers = hdrs
        }
        else{
            self.headers = RequestParams.basic()
        }
    }
    
    class func basic() -> JSONData {
        var header = JSONData()
        
        header["Accept"]        = "application/json"
        header["Content-Type"]  = "application/json"
        
        return header
    }
    
    func method() -> String?  {
        
        switch (self.HTTPMethod) {
        case .GET?: return "GET"
        case .PUT?: return "PUT"
        case .POST?: return "POST"
        case .DELETE?: return "DELETE"
        case .none: break
        }
        
        return nil
    }
    
}
