//
//  CredentialsView.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 04/08/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//

import UIKit

class CredentialsView: UIViewNib {

    @IBOutlet weak var appIdContainer: UIView!
    @IBOutlet weak var devKeyContainer: UIView!
    @IBOutlet weak var buttonContainer: UIView!
    
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var appIdTF: UITextField!
    @IBOutlet weak var devKeyTF: UITextField!
    
    var delegate: CredentialsViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initializeNib()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initializeNib()
    }
    
    override func initializeNib() {
        super.initializeNib()
        
        setUp()
    }
    
    func setUp() {
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        appIdContainer.addDropShadow()
        devKeyContainer.addDropShadow()
        buttonContainer.addDropShadow()
    }
    
    @IBAction func backBtnPushed(_ sender: UIButton) {
        delegate?.popVC()
    }
    
    @IBAction func continueButtonPushed(_ sender: UIButton) {
        guard let appId: String  = self.appIdTF.text, appId.isValid() else { return }
        guard let devKey: String = self.devKeyTF.text, devKey.isValid() else { return }
        
        let credential: JSONData = ["appId": appId.strip(), "developerKey": devKey.strip()]
        
        delegate?.buttonPushed(credential: credential)
    }
}

extension String {
    func strip() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
}
