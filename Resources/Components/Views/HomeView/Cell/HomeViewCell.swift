//
//  HomeViewCell.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 05/08/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//

import UIKit

class HomeViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    
    @IBOutlet weak var container: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        container.addDropShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension HomeViewCell {
    func title(_ title: String, _ subtitle: String) {
        self.title.text = title.uppercased()
        self.subtitle.text = subtitle
    }
}
