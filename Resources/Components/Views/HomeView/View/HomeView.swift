//
//  HomeView.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 05/08/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//

import UIKit

class HomeView: UIViewNib {
    static let reuseableIdentifier = "HomeViewCellIdentifier"
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewContainer: UIView!
    @IBOutlet weak var buttonContainer: UIView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var tableViewContainerHeight: NSLayoutConstraint!
    
    var info: RoadInfo?
    var delegate: HomeViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initializeNib()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initializeNib()
    }
    
    override func initializeNib() {
        super.initializeNib()
        
        setUp()
    }
    
    func setUp() {
        tableView.register(UINib.init(nibName: "HomeViewCell", bundle: nil), forCellReuseIdentifier: HomeView.reuseableIdentifier)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        buttonContainer.addDropShadow()
    }
    
    @IBAction func searchBtnPushed(_ sender: UIButton) {
        searchTF.resignFirstResponder()
        
        guard let road = searchTF.text, road.isValidRoad() else { return }
        
        delegate?.getInfo(for: road, completion: { [weak self] (info) in
            
            guard let info = info else {
                return
            }
            
            self?.info = info
            
            UIView.animate(withDuration: 0.2, animations: {
                
                self?.tableViewContainerHeight = self?.tableViewContainerHeight.setMultiplier(multiplier: 0.42)
                
            }, completion: { (_) in
                
                self?.tableView.reloadData()
            })
        })
    }
    
    @IBAction func credentialsBtnPushed(_ sender: UIButton) {
        delegate?.getCredentials()
    }
    
    func configureCell(cell: HomeViewCell, indexPath: IndexPath) {
        
        if (info?.isSuccessful)! {
            switch SuccessInfoOrder(rawValue: indexPath.row)!  {
            
                case .id:                           cell.title("id:", info?.id as! String)
                case .url:                          cell.title("url:", info?.url as! String)
                case .type:                         cell.title("type:", info?.type as! String)
                case .bounds:                       cell.title("bounds:", info?.bounds as! String)
                case .envelope:                     cell.title("envelope:", info?.envelope as! String)
                case .displayName:                  cell.title("Display Name:", info?.displayName as! String)
                case .statusSeverity:               cell.title("Status Severity:", info?.statusSeverity as! String)
                case .statusSeverityDescription:    cell.title("Status Severity Description:", info?.statusSeverityDescription as! String)
            }
        }
        else {
            switch FailureInfoOrder(rawValue: indexPath.row)!  {
                
                case .type:             cell.title("type:", info?.type as! String)
                case .message:          cell.title("Message:", info?.message as! String)
                case .httpStatus:       cell.title("httpStatus:", info?.httpStatus as! String)
                case .relativeUri:      cell.title("relativeUri:", info?.relativeUri as! String)
                case .timestampUtc:     cell.title("timestampUtc:", info?.timestampUtc as! String)
                case .exceptionType:    cell.title("exceptionType:", info?.exceptionType as! String)
                case .httpStatusCode:   cell.title("httpStatusCode:", "\(String(describing: info?.httpStatusCode))")
            }
        }
    }
}

extension HomeView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let info = self.info {
            return info.isSuccessful ? SuccessInfoOrder.requirementCount : FailureInfoOrder.requirementCount
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HomeView.reuseableIdentifier, for: indexPath) as! HomeViewCell
        
        configureCell(cell: cell, indexPath: indexPath)
        return cell
    }
}
