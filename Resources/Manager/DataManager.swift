//
//  DataManager.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 04/08/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//

import UIKit
import CoreData

final class DataManager {
    
    private lazy var dataStack: CoreDataCommons = {
        return CoreDataCommons.sharedInstance
    }()
    
    private let baseURL: String
    private var credentials: Credentials?
    
    // MARK:- Init -
    
    init(baseURL: String) {
        self.baseURL = baseURL
    }
    
    // MARK:- API -
    
    func fetchInfo(_ road: String, completion: @escaping RoadStatCompletion) {
        
        if let credentials: [Credentials] = dataStack.get() {
            self.credentials = credentials.first
        }
        
        let road: Endpoint = roadEp(road)
    
        if road.error != nil {
            completion(road.error, nil)
        }
        else {
            road.endpoint?.get(parameters: RequestParams(baseURL: API.BaseURL)) { [weak self]  (response)  in
                
                self?.fetchDidFinish(response, completion: completion)
            }
        }
    }
    
    // MARK:- Helper -
    
    private func fetchDidFinish(_ response: Response, completion: @escaping RoadStatCompletion) {

        if let _ = response.error {
            completion( .FailedRequest, nil)
        }
        else if let data = format(json: response.prettyPrinted) as JSONData? {
            
            if response.ok! {
                completion( nil, RoadInfo(data: data))
                
                self.save([data], as: Roads.self)
            }
            else {
                completion( .InvalidResponse, RoadInfo(data: data))
            }
        }
        else{
            completion(.FailedRequest, nil)
        }
    }
    
    private func format(json: JSON? ) -> JSONData? {
        
        if json is JSONData {
            return json as? JSONData
        }
        else if json is [JSON] {
            return (json as! Array).first
        }
        
        return nil
    }
    
    
    // Road Endpoint
    
    private func roadEp(_ road: String) -> Endpoint {
        
        guard let road   = road as String?, road.isValid() else { return ( .InvalidRoad, nil) }
        guard let appId  = self.credentials?.appId, appId.isValid() else { return ( .CredentialNotFound, nil) }
        guard let devKey = self.credentials?.developerKey, devKey.isValid() else { return ( .CredentialNotFound, nil) }
        
        return ( nil, String(format:API.Status, road.uppercased(), appId, devKey))
    }
    
    
    // MARK:- DB Data -
    
    func get<T: NSManagedObject>(completion: @escaping (Error? , [T]?) -> ()) {

        self.dataStack.get { (error, data: [T]?) in
            completion(error, data)
        }
    }
    
    func save<T: NSManagedObject>(_ object: [JSONData], as: T.Type) {
        DispatchQueue.global(qos: .background).async {
            
            guard let object = object as [JSONData]? else { return }
            
            self.dataStack.insert(object, completion: { (error, info: T?) in
            })
        }
    }
    
    // Only 1 developer credentials store at a time
    func saveCredentials(_ object: JSONData, completion: @escaping () -> () = { }) {
        
        dataStack.delete { [weak self] (error, cred: Credentials?) in
            
            self?.save([object], as: Credentials.self)
        }
    }
    
    class func cast<T: NSManagedObject>(_ objects: [T]) -> RoadInfo? {
        
        var buffer  = [JSONData]()
        let keys    = Array(T.entity().attributesByName.keys)
        
        objects.forEach {
            buffer.append($0.dictionaryWithValues(forKeys: keys))
        }
        
        return RoadInfo(data: buffer)
    }

}
