//
//  CredentialVCTest.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 28/07/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//

import XCTest
@testable import TFLPortal

class DetailVCTest: XCTestCase {
    
    var credentialVC: CredentialsVC!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        credentialVC = storyboard.instantiateViewController(withIdentifier: "CredentialsVC") as! CredentialsVC
        
        _ = credentialVC.view
    }
    
    override func tearDown() {
        credentialVC =  nil
        super.tearDown()
    }
    
    func testViewLoaded(){
        XCTAssertNotNil(credentialVC)
    }
    
    func testInitDetailView() {
        XCTAssertNotNil(credentialVC.credentialsView)
    }
  
    func testCredentialsViewDelegate() {
        XCTAssertTrue(credentialVC.credentialsView.delegate is CredentialsVC)
    }
}
