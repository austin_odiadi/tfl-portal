//
//  HomeVCTest.swift
//  TFLPortalTests
//
//  Created by Austin Odiadi on 28/07/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//

import XCTest
@testable import TFLPortal

class HomeVCTest: XCTestCase {
    
     var homeVC: HomeVC!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        
        _ = homeVC.view
    }
    
    override func tearDown() {
        homeVC = nil
        super.tearDown()
        
    }
    
    func testViewLoaded(){
        XCTAssertNotNil(homeVC)
    }
    
    func testInitHomeView() {
        XCTAssertNotNil(homeVC.homeView)
    }
    
    func testTableViewViewLoaded(){
        XCTAssertNotNil(homeVC.homeView.tableView)
    }
    
    func testHomeViewTableViewDataSource() {
        XCTAssertTrue(homeVC.homeView.tableView.dataSource is HomeView)
    }
    
    func testHomeViewTableViewDelegate() {
        XCTAssertTrue(homeVC.homeView.tableView.delegate is HomeView)
    }
    
    func testTableViewCellCreateWithReusabelIdentifier() {
        let cell = homeVC.homeView.tableView.cellForRow(at: IndexPath(row:0, section:0))
        guard let gcell = cell else {return}
        
        XCTAssertTrue(gcell.reuseIdentifier == HomeView.reuseableIdentifier)
    }
    
    func testHomeViewDelegate() {
        XCTAssertTrue(homeVC.homeView.delegate is HomeVC)
    }
    
    func testHomeVCLoadData() {
    }
}
