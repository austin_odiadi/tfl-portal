//
//  CredentialsVC.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 04/08/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//

import UIKit

protocol CredentialsViewDelegate {
    func popVC()
    func buttonPushed(credential: JSONData)
}

class CredentialsVC: BaseVC {

    @IBOutlet weak var credentialsView: CredentialsView!
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    var type: VCPush = .none {
        didSet {
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        credentialsView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateCredential(cred: JSONData) {
        self.navigationController?.viewControllers.forEach {
            
            if $0 is HomeVC {
                
                let homeVC = $0 as! HomeVC
                homeVC.credential = cred
                
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if type == .home {
            credentialsView?.backButton.isHidden = false
        }
        else {
            credentialsView?.backButton.isHidden = true
        }
    }
}

extension CredentialsVC: CredentialsViewDelegate {
    func popVC() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func buttonPushed(credential: JSONData) {
        
        switch type {
        case .home:
            updateCredential(cred: credential)
            break
            
        default:
            self.push(.home, credential as AnyObject)
            break
        }
        
        UserDefaults.standard.set(true, forKey: "hasKey")
    }
}
