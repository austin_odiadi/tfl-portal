//
//  SplashVC.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 04/08/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//

import UIKit

class SplashVC: BaseVC {

    @IBOutlet weak var label: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let animate = { [unowned self] in
            self.label.alpha = 1.0
        }
        
        UIView.animate(withDuration: kSplashDuration, animations: animate )
        { [unowned self] (_)  in
            let hasKey: Bool = UserDefaults.standard.bool(forKey: "hasKey")
            
            if hasKey {
                self.push( .home)
            }
            else {
                self.push( .credential, VCPush.splash)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}
