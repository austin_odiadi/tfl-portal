//
//  HomeVC.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 04/08/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//

import UIKit

protocol HomeViewDelegate {
    func getCredentials()
    func getInfo(for road: String, completion: @escaping (RoadInfo?) -> () )
}

class HomeVC: BaseVC {

    var credential: JSONData?
    @IBOutlet weak var homeView: HomeView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        homeView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        saveCredentialIfAny()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func saveCredentialIfAny() {
        guard let credentials = self.credential as JSONData? else { return }
        
        dataManager.saveCredentials(credentials)
    }
}

extension HomeVC: HomeViewDelegate {
    func getInfo(for road: String, completion: @escaping (RoadInfo?) -> ()) {
        
        self.dataManager.fetchInfo(road) { [weak self] (error: DataManagerError?, info: RoadInfo?) in
            
            if let message = error?.errorMessage(), info == nil {
                self?.alert("Error", message)
            }
            completion(info)
        }
    }
    
    func getCredentials() {
        push(.credential,  VCPush.home)
    }
}
