//
//  RoadInfo.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 04/08/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//

import UIKit

struct RoadInfo {
    
    let id:                         Any?
    let url:                        Any?
    let type:                       Any?
    let bounds:                     Any?
    let message:                    Any?
    let envelope:                   Any?
    let httpStatus:                 Any?
    let relativeUri:                Any?
    let displayName:                Any?
    let timestampUtc:               Any?
    let exceptionType:              Any?
    let statusSeverity:             Any?
    let httpStatusCode:             Any?
    let statusSeverityDescription:  Any?
    
    let isSuccessful: Bool
}

extension RoadInfo: JSONDecodable {
    
    init?(data: JSON) {
        guard var data = data as? JSONData else { return nil }
        
        id                          = data["id"] ?? nil
        url                         = data["url"] ?? nil
        type                        = data["$type"] ?? nil
        bounds                      = data["bounds"] ?? nil
        message                     = data["message"] ?? nil
        envelope                    = data["envelope"] ?? nil
        httpStatus                  = data["httpStatus"] ?? nil
        relativeUri                 = data["relativeUri"] ?? nil
        displayName                 = data["displayName"] ?? nil
        timestampUtc                = data["timestampUtc"] ?? nil
        exceptionType               = data["exceptionType"] ?? nil
        statusSeverity              = data["statusSeverity"] ?? nil
        httpStatusCode              = data["httpStatusCode"] ?? nil
        statusSeverityDescription   = data["statusSeverityDescription"] ?? nil
        
        isSuccessful = message == nil
    }
}

extension RoadInfo
{
    func properties() -> [String] {
        return Mirror(reflecting: self).children.flatMap { $0.label }
    }
}

// MARK:- Private
extension RoadInfo {
    
    func setNotNil<T>( variable:inout T, _ value: T?) {
        
        if value != nil {
            variable = value!
        }
    }
}
