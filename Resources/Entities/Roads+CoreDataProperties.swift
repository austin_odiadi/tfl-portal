//
//  Roads+CoreDataProperties.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 05/08/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//
//

import Foundation
import CoreData


extension Roads {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Roads> {
        return NSFetchRequest<Roads>(entityName: "Roads")
    }


}
