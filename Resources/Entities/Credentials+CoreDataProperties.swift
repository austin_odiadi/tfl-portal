//
//  Credentials+CoreDataProperties.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 05/08/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//
//

import Foundation
import CoreData


extension Credentials {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Credentials> {
        return NSFetchRequest<Credentials>(entityName: "Credentials")
    }

    @NSManaged public var developerKey: String?
    @NSManaged public var appId: String?

}
