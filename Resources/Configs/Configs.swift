//
//  ViewConfigs.swift
//  TFLPortal
//
//  Created by Austin Odiadi on 04/08/2018.
//  Copyright © 2018 Austin Odiadi. All rights reserved.
//

import Foundation

// View
let kSplashDuration = 3.0


// Session
let kSessiontimeOut: UInt = 60

// Alias
typealias JSON = Any
typealias JSONData = [String: JSON]
typealias Endpoint = (error: DataManagerError? , endpoint: String?)
typealias RoadStatCompletion = (DataManagerError? , RoadInfo?) -> ()

//API
enum API {
    
    static let Status: String  = "%@?app_id=%@&app_key=%@"
    static let BaseURL: String = "https://api.tfl.gov.uk/Road/"
}

// DataManager Error
enum DataManagerError: Error {
    
    case Unknown
    
    // Post Request
    case FailedRequest
    case InvalidResponse
    
    // Pre Request
    case InvalidRoad
    case CredentialNotFound
    
    func errorMessage() -> String {
        switch self {
        case .FailedRequest:
            return "Request failed."
        case .InvalidResponse:
            return "Invalid response."
            
        case .InvalidRoad:
            return "Invalid road."
        case .CredentialNotFound:
            return "Credentials not found."
            
        case .Unknown:
            return "Unknown error."
        }
    }
}

// VCs
enum ViewControllers : Int {
    case splash
    case home
    case credential
}

// Push Type
enum VCPush : Int {
    case none
    case splash
    case home 
}

// Data
enum FailureInfoOrder : Int {
    
    case message        = 0
    case timestampUtc   = 1
    case exceptionType  = 2
    case httpStatusCode = 3
    case httpStatus     = 4
    case relativeUri    = 5
    case type           = 6
    
    static var requirementCount: Int { return 1 }
    static var count: Int { return FailureInfoOrder.type.hashValue + 1 }
}

enum SuccessInfoOrder : Int {
    
    case displayName                = 0
    case statusSeverity             = 1
    case statusSeverityDescription  = 2
    case id                         = 3
    case bounds                     = 4
    case envelope                   = 5
    case url                        = 6
    case type                       = 7
    
    static var requirementCount: Int { return 3 }
    static var count: Int { return SuccessInfoOrder.type.hashValue + 1 }
}
